// #include <vector>
// #include <memory>
#include <iostream>

// #include "student.h"
#include "database.h"

using namespace emumba::training;

database::database()
{
    students[0].set_student_data("student1", "student1", 11, 3.11);
    students[1].set_student_data("student2", "student2", 12, 3.12);
    students[2].set_student_data("student3", "student3", 13, 3.13);
    students[3].set_student_data("student4", "student4", 14, 3.14);
    students[4].set_student_data("student5", "student5", 15, 3.15);
    students[5].set_student_data("student6", "student6", 16, 3.16);
}

std::shared_ptr<student> database::get_student_reference_shared(const std::string &student_name)
{
    for (int iter = 0; iter != students.size(); ++iter)
    {
        if (student_name == students[iter].get_name())
        {
            std::cout << "name found\n";
            return std::make_shared<student>(students[iter]);
        }
    }
    return nullptr;
}

std::unique_ptr<student> database::get_student_reference_unique(const std::string &student_name)
{
    for (int iter = 0; iter != students.size(); ++iter)
    {
        if (student_name == students[iter].get_name())
        {
            std::cout << "name found\n";
            return std::make_unique<student>(students[iter]);
        }
    }
    return nullptr;
}