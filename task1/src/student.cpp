// #include <string>
// #include <map>
#include <iostream>

#include "student.h"

using namespace emumba::training;
// todos
// learn about logs and convert cout statments into logs
// if set conditions fail on records what are defaults

const int student::max_cgpa = 4;
const int student::max_marks = 100;
// int count = 0;

student::student()
{
    std::cout << "student() constructor called\n";
    set_name("no name");
    set_cgpa(0.0);
    set_roll_no("roll_no not found");
    // set_age(++count);
}

student::student(const std::string& name, const std::string& roll_no, const int& age, const float& cgpa)
{
    std::cout << "student(name, roll_no, age, cgpa) constructor called\n";
    set_name(name);
    set_cgpa(cgpa);
    set_roll_no(roll_no);
    set_age(age);
}

student::~student()
{
    std::cout << get_name() << get_age() <<" student() destructor called \n";
}

int student::get_subject_marks(const std::string& subject)
{
    map_iter = result.find(subject);
    if (map_iter == result.end())
        return 0;
    return map_iter->second;
}
void student::set_subject_marks(const std::string& subject, const int& marks)
{
    //marks must be in 0 to 100 range
    if (marks > max_marks || marks < 0)
    {
        std::cout << subject << " marks not set due to error: \n\t marks must be within 0 and 100.\n";
        return;
    }

    //check if subject already exists
    if (result.find(subject) == result.end())
        std::cout << subject << " marks set succesfully.\n";
    else
        std::cout << subject << " marks updated succesfully.\n";

    result[subject] = marks;
}

void student::print_all_marks()
{
    std::cout << "printing marks\n";
    for (map_iter = result.begin(); map_iter != result.end(); ++map_iter)
    {
        std::cout << map_iter->first << "  =  " << map_iter->second << " \n";
    }
    std::cout << "printing completed\n";
}
std::string student::print_student_data()
{
    std::string ret_string;
    ret_string =  "Name     :    " + get_name() + "\n";
    ret_string += "roll no  :    " + get_roll_no() + "\n";
    ret_string += "age      :    " + std::to_string(get_age()) + "\n";
    ret_string += "cgpa     :    " + std::to_string(get_cgpa()) + "\n";

    return ret_string;
}
void student::set_student_data(const std::string& name, const std::string& roll_no, const int& age, const float& cgpa)
{
    // std::cout << "student(name, roll_no, age, cgpa) constructor called\n";
    set_name(name);
    set_cgpa(cgpa);
    set_roll_no(roll_no);
    set_age(age);
}

std::string student::get_name()
{
    return record.name;
}
void student::set_name(const std::string& name)
{
    record.name = name;
}

int student::get_age()
{
    return record.age;
}
void student::set_age(const int& age)
{
    // age limit condition
    if (age < 2)
    {
        record.age = 2;
        std::cout << "students with age lower than 2 are not allowed in school\n";
    }
    else
        record.age = age;
}

std::string student::get_roll_no()
{
    return record.roll_no;
}
void student::set_roll_no(const std::string& roll_no)
{
    // roll_no conditions
    record.roll_no = roll_no;
}

float student::get_cgpa()
{
    return record.cgpa;
}
void student::set_cgpa(const float& cgpa)
{
    // cgpa conditions
    if (cgpa >= 0 and cgpa <= max_cgpa)
        record.cgpa = cgpa;
    else
    {
        std::cout << "cgpa must be within 0 and 4.\n";
        record.cgpa = 0;
    }
}
