#include <iostream>

// #include "student.h"
#include "database.h"

using namespace emumba::training;

void print_student_record(const std::unique_ptr<student> &student_ptr)
{
    std::cout << "unique ptr\n" << student_ptr->print_student_data();
    student_ptr->print_all_marks();
}
void print_student_record(std::shared_ptr<student> student_ptr)
{
    std::cout << "shared ptr\n" << student_ptr->print_student_data();
    student_ptr->print_all_marks();
}

int main()
{
    database mydata;
    std::shared_ptr<student> shared_student = mydata.get_student_reference_shared("student1");
    std::unique_ptr<student> unique_student = mydata.get_student_reference_unique("student1");

    // to check if return value is nullptr
    if (shared_student)
    {
        std::cout << std::endl
                  << "shared ptr" << std::endl;
        shared_student->print_student_data();
        shared_student->print_all_marks();
        shared_student->set_subject_marks("maths", 82);
        shared_student->set_subject_marks("computer", 99);
    }
    if (unique_student)
    {
        std::cout << std::endl
                  << "unique ptr" << std::endl;
        unique_student->print_student_data();
        unique_student->print_all_marks();
    }

    shared_student = mydata.get_student_reference_shared("student5");
    unique_student = mydata.get_student_reference_unique("student5");
    if (shared_student)
    {
        std::cout << std::endl
                  << "shared ptr" << std::endl;
        shared_student->print_all_marks();
        std::cout << shared_student.get() << std::endl;
        std::cout << shared_student.use_count() << "\n\n";
    }
    if (unique_student)
    {
        std::cout << std::endl
                  << "unique ptr" << std::endl;
        unique_student->print_all_marks();
        std::cout << unique_student.get() << std::endl;
    }

    print_student_record(mydata.get_student_reference_shared("student1"));
    print_student_record(mydata.get_student_reference_unique("student1"));

    return 0;
}