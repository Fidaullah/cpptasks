#include <vector>
#include <memory>
#include <list>
#include "student.h"

namespace emumba::training
{
    class database
    {
    private:
        std::vector<student> students = std::vector<student>(6);
        std::list<std::shared_ptr<student>> students_ptr; // = std::list<std::shared_ptr<student>>(6);

    public:
        database();
        std::shared_ptr<student> get_student_reference_shared(const std::string &student_name);
        std::unique_ptr<student> get_student_reference_unique(const std::string &student_name);
    };
}