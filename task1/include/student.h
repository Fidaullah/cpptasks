#pragma once

#include <string>
#include <map>


namespace emumba::training
{
class student
{
    private:
        struct student_record
        {
            std::string name;
            std::string roll_no;
            int age;
            float cgpa;
        };
        
        student_record record;
        static const int max_cgpa; // can 4.0 or 5.0
        static const int max_marks;
        std::map<std::string /*subject name*/, int /*marks*/> result;
        std::map<std::string, int>::iterator map_iter;

    public:
        student();
        student(const std::string& name, const std::string& roll_no, const int& age, const float& cgpa);

        ~student();

        int get_subject_marks(const std::string& subject);
        void set_subject_marks(const std::string& subject, const int& marks);
        void print_all_marks();
        
        std::string print_student_data();
        void set_student_data(const std::string& name, const std::string& roll_no, const int& age, const float& cgpa);

        std::string get_name();
        void set_name(const std::string& name);

        int get_age();
        void set_age(const int& age);

        std::string get_roll_no();
        void set_roll_no(const std::string& roll_no);

        float get_cgpa();
        void set_cgpa(const float& cgpa);



        /*
        Implement your get and set functions here to read/write the roll_no,
        age and cgpa.
        You can declare any private variables too if necessary.
        */
};

}