# cpptasks
this repository is for cpp tasks and practice

To clone this repository
```
git clone https://gitlab.com/Fidaullah/cpptasks.git
```
for review checkout to **project_review** branch

for advance topic task checkout to **advance_topics** branch

To checkout to specific branch 

```git checkout <branch_name> ```


## task1
this task follows creating c project part 1

link : https://trello.com/c/d3yjck6h/234-creating-a-c-project-part-1

further tasks from link : https://trello.com/c/pMKqEbeW/245-smart-pointers-in-c

to build project 

```
cd to task1/
mkdir build
cd build
cmake ..
make
```

to rum program 
```
./app/myprogram
```
